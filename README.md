# RDS 81346

## Getting started with CSV importer

```
cd csv2ontology
python3 -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
```
## Update ontology

Go to .owl file to  update and remove all obsolete entities. After that run script by: python main.py -c [input.csv] -o [ontology.owl].

Example:

```
cd python
python main.py -c EgpMainSystems.csv -o egpSystems.owl
python main.py -c EgpSystems.csv -o egpSystems.owl
python main.py -c EgpComponents.csv -o egpComponents.owl
python main.py -c PowerSupplySystems81346-10.csv -o rds81346-10.owl
python main.py -c TechnicalSystems81346-10.csv -o rds81346-10.owl
python main.py -c ComponentSystems81346-2.csv -o rds81346-2.owl
```

## Ontop CLI materialization

./ontop materialize -m ~/Desktop/Repos/rds81346/egpCustom/egpCustom.obda -p ~/Desktop/Repos/rds81346/egpCustom/egpCustom.properties -f turtle -o ~/Desktop/Repos/rds81346/egpCustom/export/cliexportfull.ttl


## Jena bug resolution

After every container deletion:
```
docker exec -it fuseki bash

root@7fc3133f5863:/jena-fuseki# apt-get update
root@7fc3133f5863:/jena-fuseki# apt-get install -y procps
root@7fc3133f5863:/jena-fuseki# exit
```
