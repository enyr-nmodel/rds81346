[QueryItem="Dams"]
PREFIX : <https://gitlab.com/enyr-nmodel/rds81346/-/raw/main/egpCustom/egpCustom.owl#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX xml: <http://www.w3.org/XML/1998/namespace>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX obda: <https://w3id.org/obda/vocabulary#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX rds1: <https://gitlab.com/enyr-nmodel/rds81346/-/raw/main/rds81346-1.owl#>
PREFIX rds10: <https://gitlab.com/enyr-nmodel/rds81346/-/raw/main/rds81346-10.owl#>
PREFIX rds2: <https://gitlab.com/enyr-nmodel/rds81346/-/raw/main/rds81346-2.owl#>
PREFIX egpco: <https://gitlab.com/enyr-nmodel/rds81346/-/raw/main/egpComponents.owl#>
PREFIX egpms: <https://gitlab.com/enyr-nmodel/rds81346/-/raw/main/egpMainSystems.owl#>
PREFIX egpsy: <https://gitlab.com/enyr-nmodel/rds81346/-/raw/main/egpSystems.owl#>

SELECT ?gate ?label ?height ?width ?year ?lc ?inst
WHERE
{
  ?gate a rds1:ProductIndividual .
  ?gate rdfs:label ?label .
  ?gate :Width ?width .
  ?gate :Height ?height .
  ?gate :ManufacturingYear ?year .
  ?gate rds1:UsedFor ?po .
  ?sld rds1:Designates ?po .
  ?sld a rds1:SingleLevelReferenceDesignation .
  ?sld rds1:SequenceNumber ?inst .
  ?sld rds1:LetterCodeClass enelco:PowerPlantComponentQP .
  enelco:PowerPlantComponentQP rds1:LetterCode ?lc .
}
[QueryItem="GatesWithDesignation"]
PREFIX : <https://gitlab.com/enyr-nmodel/rds81346/-/raw/main/egpCustom/egpCustom.owl#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX xml: <http://www.w3.org/XML/1998/namespace>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX obda: <https://w3id.org/obda/vocabulary#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX rds1: <https://gitlab.com/enyr-nmodel/rds81346/-/raw/main/rds81346-1.owl#>
PREFIX rds10: <https://gitlab.com/enyr-nmodel/rds81346/-/raw/main/rds81346-10.owl#>
PREFIX rds2: <https://gitlab.com/enyr-nmodel/rds81346/-/raw/main/rds81346-2.owl#>
PREFIX egpco: <https://gitlab.com/enyr-nmodel/rds81346/-/raw/main/egpComponents.owl#>
PREFIX egpms: <https://gitlab.com/enyr-nmodel/rds81346/-/raw/main/egpMainSystems.owl#>
PREFIX egpsy: <https://gitlab.com/enyr-nmodel/rds81346/-/raw/main/egpSystems.owl#>

SELECT ?gate ?label ?height ?width ?year ?topid ?lcbl0 ?sqbl0 ?lcbl1 ?sqbl1 ?lcbl2 ?sqbl2 ?top
WHERE
{
  ?gate a rds1:ProductIndividual .
  ?gate rdfs:label ?label .
  ?gate :Width ?width .
  ?gate :Height ?height .
  ?gate :ManufacturingYear ?year .
  ?gate rds1:UsedFor ?po .
  ?mldbl2 rds1:Designates ?po .
  ?mldbl2 a rds1:MultiLevelReferenceDesignation .
  ?mldbl2 rds1:ComposedOf ?sldbl2 .
  ?sldbl2 rds1:LetterCodeClass ?clbl2 .
  ?sldbl2 rds1:SequenceNumber ?sqbl2 .
  ?clbl2 rds1:LetterCode ?lcbl2 .
  ?mldbl2 rds1:ComposedOfPrevious ?mldbl1 .
  ?mldbl1 rds1:ComposedOf ?sldbl1 .
  ?sldbl1 rds1:SequenceNumber ?sqbl1 .
  ?sldbl1 rds1:LetterCodeClass ?clbl1 .
  ?clbl1 rds1:LetterCode ?lcbl1 .
  ?mldbl1 rds1:ComposedOfPrevious ?mldbl0 .
  ?mldbl0 rds1:ComposedOf ?sldbl0 .
  ?sldbl0 rds1:SequenceNumber ?sqbl0 .
  ?sldbl0 rds1:LetterCodeClass ?clbl0 .
  ?clbl0 rds1:LetterCode ?lcbl0 .
  ?mldbl0 rds1:ComposedOfPrevious ?top .
  ?top rds1:Identifier ?topid
}
[QueryItem="GatesWithDesignationAlternative"]
PREFIX : <https://gitlab.com/enyr-nmodel/rds81346/-/raw/main/egpCustom/egpCustom.owl#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX xml: <http://www.w3.org/XML/1998/namespace>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX obda: <https://w3id.org/obda/vocabulary#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX rds1: <https://gitlab.com/enyr-nmodel/rds81346/-/raw/main/rds81346-1.owl#>
PREFIX rds10: <https://gitlab.com/enyr-nmodel/rds81346/-/raw/main/rds81346-10.owl#>
PREFIX rds2: <https://gitlab.com/enyr-nmodel/rds81346/-/raw/main/rds81346-2.owl#>
PREFIX egpco: <https://gitlab.com/enyr-nmodel/rds81346/-/raw/main/egpComponents.owl#>
PREFIX egpms: <https://gitlab.com/enyr-nmodel/rds81346/-/raw/main/egpMainSystems.owl#>
PREFIX egpsy: <https://gitlab.com/enyr-nmodel/rds81346/-/raw/main/egpSystems.owl#>

SELECT ?gate ?label ?height ?width ?year ?topid ?lcbl0 ?sqbl0 ?lcbl1 ?sqbl1 ?lcbl2 ?sqbl2
WHERE
{
  ?gate a rds1:ProductIndividual .
  ?gate rdfs:label ?label .
  ?gate :Width ?width .
  ?gate :Height ?height .
  ?gate :ManufacturingYear ?year .
  ?gate rds1:UsedFor ?po .
  ?sldbl2 rds1:Designates ?po .
  ?sldbl2 rds1:SequenceNumber ?sqbl2 .
  ?sldbl2 rds1:LetterCodeClass ?clbl2 .
  ?clbl2 rds1:LetterCode ?lcbl2 .
  ?bl2set rds1:DesignationOfTheObjectOccurrence ?sldbl2 .
  ?bl2set rds1:DesignationOfAncestorObjectOccurrence ?sldbl1  .
  ?sldbl1 rds1:SequenceNumber ?sqbl1 .
  ?sldbl1 rds1:LetterCodeClass ?clbl1 .
  ?clbl1 rds1:LetterCode ?lcbl1 .
  ?bl1set rds1:DesignationOfTheObjectOccurrence ?sldbl1 .
  ?bl1set rds1:DesignationOfAncestorObjectOccurrence ?sldbl0  .
  ?sldbl0 rds1:SequenceNumber ?sqbl0 .
  ?sldbl0 rds1:LetterCodeClass ?clbl0 .
  ?clbl0 rds1:LetterCode ?lcbl0 .
  ?bl0set rds1:DesignationOfTheObjectOccurrence ?sldbl0 .
  ?bl0set rds1:AssociatedTo ?objbl0 .
  ?objbl0 rds1:HasTopNodeIdentifier ?top .
  ?top rds1:Identifier ?topid .

}
[QueryItem="IndividualsByClass"]
PREFIX : <https://gitlab.com/enyr-nmodel/rds81346/-/raw/main/egpCustom/egpCustom.owl#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX xml: <http://www.w3.org/XML/1998/namespace>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX obda: <https://w3id.org/obda/vocabulary#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX rds1: <https://gitlab.com/enyr-nmodel/rds81346/-/raw/main/rds81346-1.owl#>
PREFIX rds10: <https://gitlab.com/enyr-nmodel/rds81346/-/raw/main/rds81346-10.owl#>
PREFIX rds2: <https://gitlab.com/enyr-nmodel/rds81346/-/raw/main/rds81346-2.owl#>
PREFIX egpco: <https://gitlab.com/enyr-nmodel/rds81346/-/raw/main/egpComponents.owl#>
PREFIX egpms: <https://gitlab.com/enyr-nmodel/rds81346/-/raw/main/egpMainSystems.owl#>
PREFIX egpsy: <https://gitlab.com/enyr-nmodel/rds81346/-/raw/main/egpSystems.owl#>

SELECT ?label ?w
WHERE
{
  ?obj rds1:ClassifiedAs enelco:PowerPlantComponentQP .
  ?po rds1:ViewOf ?obj .
  ?po a rds1:ProductOccurrence .
  ?pi rds1:UsedFor ?po .
  ?pi rdfs:label ?label .
  ?pi :Width ?w .
}
[QueryItem="Registry"]
PREFIX : <https://gitlab.com/enyr-nmodel/rds81346/-/raw/main/egpCustom/egpCustom.owl#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX xml: <http://www.w3.org/XML/1998/namespace>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX obda: <https://w3id.org/obda/vocabulary#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX rds1: <https://gitlab.com/enyr-nmodel/rds81346/-/raw/main/rds81346-1.owl#>
PREFIX rds10: <https://gitlab.com/enyr-nmodel/rds81346/-/raw/main/rds81346-10.owl#>
PREFIX rds2: <https://gitlab.com/enyr-nmodel/rds81346/-/raw/main/rds81346-2.owl#>
PREFIX egpco: <https://gitlab.com/enyr-nmodel/rds81346/-/raw/main/egpComponents.owl#>
PREFIX egpms: <https://gitlab.com/enyr-nmodel/rds81346/-/raw/main/egpMainSystems.owl#>
PREFIX egpsy: <https://gitlab.com/enyr-nmodel/rds81346/-/raw/main/egpSystems.owl#>

SELECT ?level ?topid (CONCAT(STR(?bl0lc),?bl0sn) AS ?bl0) (CONCAT(STR(?bl1lc),?bl1sn) AS ?bl1) (CONCAT(STR(?bl2lc),?bl2sn) AS ?bl2) ?desc ?comm
WHERE
{
  {
    ?bl2po :PartOf ?bl1po .
    ?bl1po :PartOf ?bl0po .
    ?bl2po a rds1:ProductOccurrence .
    ?bl2po rdfs:comment ?desc .
    ?bl2po rds1:ViewOf ?bl2obj .
    ?bl1po rds1:ViewOf ?bl1obj .
    ?bl0po rds1:ViewOf ?bl0obj .
    ?bl0obj rds1:HasTopNodeIdentifier ?top .
    ?top rds1:Identifier ?topid .
    ?bl2obj rds1:ClassifiedAs ?bl2cl .
    ?bl1obj rds1:ClassifiedAs ?bl1cl .
    ?bl0obj rds1:ClassifiedAs ?bl0cl .
    ?bl2cl a enelco:PowerPlantComponentClass .  
    OPTIONAL
    {
    	?bl2cl rdfs:comment ?comm .
    }
    ?bl2cl rds1:LetterCode ?bl2lc .
    ?bl1cl rds1:LetterCode ?bl1lc .
    ?bl0cl rds1:LetterCode ?bl0lc .
    ?bl2sld rds1:Designates ?bl2po .
    ?bl1sld rds1:Designates ?bl1po .
    ?bl0sld rds1:Designates ?bl0po .
    ?bl2sld rds1:SequenceNumber ?bl2sn .
    ?bl1sld rds1:SequenceNumber ?bl1sn .
    ?bl0sld rds1:SequenceNumber ?bl0sn .
    BIND("BL2" AS ?level).
  }
  UNION
  {
    ?bl1po :PartOf ?bl0po .
    ?bl1po a rds1:ProductOccurrence .
    ?bl1po rdfs:comment ?desc .
    ?bl1po rds1:ViewOf ?bl1obj .
    ?bl0po rds1:ViewOf ?bl0obj .
    ?bl0obj rds1:HasTopNodeIdentifier ?top .
    ?top rds1:Identifier ?topid .
    ?bl1obj rds1:ClassifiedAs ?bl1cl .
    ?bl0obj rds1:ClassifiedAs ?bl0cl .
    ?bl1cl a enelsy:PowerPlantSystemClass .
    OPTIONAL
    {
    	?bl1cl rdfs:comment ?comm .
    }
    ?bl1cl rds1:LetterCode ?bl1lc .
    ?bl0cl rds1:LetterCode ?bl0lc .
    ?bl1sld rds1:Designates ?bl1po .
    ?bl0sld rds1:Designates ?bl0po .
    ?bl1sld rds1:SequenceNumber ?bl1sn .
    ?bl0sld rds1:SequenceNumber ?bl0sn .
     BIND("BL1" AS ?level).
  }
  UNION
  {
    ?bl0po a rds1:ProductOccurrence .
    ?bl0po rdfs:comment ?desc .
    ?bl0po rds1:ViewOf ?bl0obj .
    ?bl0obj rds1:HasTopNodeIdentifier ?top .
    ?top rds1:Identifier ?topid .
    ?bl0obj rds1:ClassifiedAs ?bl0cl .
    ?bl0cl a enelms:PowerPlantMainSystemClass .
    OPTIONAL
    {
    	?bl0cl rdfs:comment ?comm .
    }
    ?bl0cl rds1:LetterCode ?bl0lc .
    ?bl0sld rds1:Designates ?bl0po .
    ?bl0sld rds1:SequenceNumber ?bl0sn .
    BIND("BL0" AS ?level).
  }
  UNION
  {
    ?topn a rds1:TopNodeIdentifier .
    ?topn rds1:Identifier ?topid .
    BIND("CDPlant" AS ?level).
  }
}

ORDER BY ?topid ?bl0lc ?bl1cl ?bl2lc
[QueryItem="ServedBy"]
PREFIX : <https://gitlab.com/enyr-nmodel/rds81346/-/raw/main/egpCustom/egpCustom.owl#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX xml: <http://www.w3.org/XML/1998/namespace>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX obda: <https://w3id.org/obda/vocabulary#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX rds1: <https://gitlab.com/enyr-nmodel/rds81346/-/raw/main/rds81346-1.owl#>
PREFIX rds10: <https://gitlab.com/enyr-nmodel/rds81346/-/raw/main/rds81346-10.owl#>
PREFIX rds2: <https://gitlab.com/enyr-nmodel/rds81346/-/raw/main/rds81346-2.owl#>
PREFIX egpco: <https://gitlab.com/enyr-nmodel/rds81346/-/raw/main/egpComponents.owl#>
PREFIX egpms: <https://gitlab.com/enyr-nmodel/rds81346/-/raw/main/egpMainSystems.owl#>
PREFIX egpsy: <https://gitlab.com/enyr-nmodel/rds81346/-/raw/main/egpSystems.owl#>

SELECT ?topid (CONCAT(STR(?ulc),?useq) AS ?unitcode) (STR("ServedBy") AS ?relation) (CONCAT(STR(?mlc),?mseq) AS ?mainsystemcode) (CONCAT(STR(?plc),?pseq) AS ?penstockcode) ?penstocksapid  
WHERE
{
  ?unit :ServedBy ?penstock .
  ?penstock :SapId ?penstocksapid .
  ?penstock :PartOf ?ms .
  ?ms rds1:ViewOf ?mobj .
  ?mobj rds1:HasTopNodeIdentifier ?top .
  ?top rds1:Identifier ?topid .
  ?msld rds1:Designates ?ms .
  ?msld rds1:LetterCodeClass ?mcl .
  ?mcl rds1:LetterCode ?mlc .
  ?msld rds1:SequenceNumber ?mseq .
  ?psld rds1:Designates ?penstock .
  ?psld rds1:LetterCodeClass ?pcl .
  ?pcl rds1:LetterCode ?plc .
  ?psld rds1:SequenceNumber ?pseq .
  ?usld rds1:Designates ?unit .
  ?usld rds1:LetterCodeClass ?ucl .
  ?ucl rds1:LetterCode ?ulc .
  ?usld rds1:SequenceNumber ?useq .
}
[QueryItem="ServedByWithRules"]
PREFIX : <https://gitlab.com/enyr-nmodel/rds81346/-/raw/main/egpCustom/egpCustom.owl#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX xml: <http://www.w3.org/XML/1998/namespace>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX obda: <https://w3id.org/obda/vocabulary#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX rds1: <https://gitlab.com/enyr-nmodel/rds81346/-/raw/main/rds81346-1.owl#>
PREFIX rds10: <https://gitlab.com/enyr-nmodel/rds81346/-/raw/main/rds81346-10.owl#>
PREFIX rds2: <https://gitlab.com/enyr-nmodel/rds81346/-/raw/main/rds81346-2.owl#>
PREFIX egpco: <https://gitlab.com/enyr-nmodel/rds81346/-/raw/main/egpComponents.owl#>
PREFIX egpms: <https://gitlab.com/enyr-nmodel/rds81346/-/raw/main/egpMainSystems.owl#>
PREFIX egpsy: <https://gitlab.com/enyr-nmodel/rds81346/-/raw/main/egpSystems.owl#>

SELECT (CONCAT(?pmslc, ?pmsinst, STR(".LPC"),?pinst) AS ?pcode) ?pcomm ?psapid (STR("Serve") AS ?relation) (CONCAT(?ulc, ?uinst) AS ?ucode ) ?ucomm
WHERE
{
	?penstock :HasLetterCode "LPC" .
    	?penstock :HasInstanceNumber ?pinst .
  	?penstock :PartOf ?pms .
  	?pms :HasLetterCode ?pmslc .
    	?pms :HasInstanceNumber ?pmsinst .
  	?penstock rdfs:comment ?pcomm .
    	?penstock :SapId ?psapid .
    	?unit :ServedBy ?penstock .
    	?unit :HasLetterCode ?ulc .
    	?unit :HasInstanceNumber ?uinst .
  	?unit rdfs:comment ?ucomm .
}
[QueryItem="NLP"]
PREFIX : <https://gitlab.com/enyr-nmodel/rds81346/-/raw/main/egpCustom/egpCustom.owl#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX xml: <http://www.w3.org/XML/1998/namespace>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX obda: <https://w3id.org/obda/vocabulary#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX rds1: <https://gitlab.com/enyr-nmodel/rds81346/-/raw/main/rds81346-1.owl#>
PREFIX rds10: <https://gitlab.com/enyr-nmodel/rds81346/-/raw/main/rds81346-10.owl#>
PREFIX rds2: <https://gitlab.com/enyr-nmodel/rds81346/-/raw/main/rds81346-2.owl#>
PREFIX egpco: <https://gitlab.com/enyr-nmodel/rds81346/-/raw/main/egpComponents.owl#>
PREFIX egpms: <https://gitlab.com/enyr-nmodel/rds81346/-/raw/main/egpMainSystems.owl#>
PREFIX egpsy: <https://gitlab.com/enyr-nmodel/rds81346/-/raw/main/egpSystems.owl#>

SELECT ?tcomm ?mscomm ?scomm ?cocomm
WHERE
{
    ?ms :HasLetterCode "M" .
    ?ms rdfs:comment ?mscomm .
    ?s rdfs:comment ?scomm .
    ?co rdfs:comment ?cocomm .
    ?s :PartOf ?ms .
    ?co :PartOf ?s .
    ?ms rds1:ViewOf ?mso .
    ?mso rds1:HasTopNodeIdentifier ?top .
    ?top rdfs:comment ?tcomm .
    ?ms rds1:RelatesTo rds1:FunctionAspect .
    ?s rds1:RelatesTo rds1:FunctionAspect . 
    ?co rds1:RelatesTo rds1:FunctionAspect .
  FILTER (?tcomm = 'Nazzano')
}
LIMIT 10000