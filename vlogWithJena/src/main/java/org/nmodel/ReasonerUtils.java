package org.nmodel;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import org.semanticweb.rulewerk.core.exceptions.RulewerkRuntimeException;
import org.semanticweb.rulewerk.core.model.api.PositiveLiteral;
import org.semanticweb.rulewerk.core.model.api.QueryResult;
import org.semanticweb.rulewerk.core.model.api.Term;
import org.semanticweb.rulewerk.core.reasoner.LiteralQueryResultPrinter;
import org.semanticweb.rulewerk.core.reasoner.QueryResultIterator;
import org.semanticweb.rulewerk.core.reasoner.Reasoner;
import org.semanticweb.rulewerk.parser.ParsingException;
import org.semanticweb.rulewerk.parser.RuleParser;

public final class ReasonerUtils {
    
    private ReasonerUtils(){}

    public static void printOutQueryAnswers(final PositiveLiteral queryAtom, final Reasoner reasoner) {
		System.out.println("Answers to query " + queryAtom + " :");
		OutputStreamWriter writer = new OutputStreamWriter(System.out);
		LiteralQueryResultPrinter printer = new LiteralQueryResultPrinter(queryAtom, writer,
				reasoner.getKnowledgeBase().getPrefixDeclarationRegistry());
		try (final QueryResultIterator answers = reasoner.answerQuery(queryAtom, false)) {
			while (answers.hasNext()) {
				printer.write(answers.next());
				writer.flush();
			}
			System.out.println("Query answers are: " + answers.getCorrectness());
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		System.out.println();
	}

	public static void printOutQueryAnswers(final String queryString, final Reasoner reasoner) {
		try {
			final PositiveLiteral query = RuleParser.parsePositiveLiteral(queryString);
			printOutQueryAnswers(query, reasoner);
		} catch (final ParsingException e) {
			throw new RulewerkRuntimeException(e.getMessage(), e);
		}
	}

	public static QueryResultIterator triplesIterator(final Reasoner reasoner, String s, String p, String o) {
		try{
			s = s==null? "?X" : s;
			p = p==null? "?Y" : p;
			o = o==null? "?Z" : o;
			QueryResultIterator it = reasoner.answerQuery(
				RuleParser.parsePositiveLiteral("TI("+s+","+p+","+o+")"), true);
			return it;
		} catch (final ParsingException e) {
			throw new RulewerkRuntimeException(e.getMessage(), e);
		}
	}

	public static void saveTriplesToNTriples(final Reasoner reasoner, final String filePath) throws IOException{

		try(PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(filePath)))){

			QueryResultIterator it = triplesIterator(reasoner, null, null, null);
			while(it.hasNext()){

				QueryResult res = it.next();
				for(Term t : res.getTerms()){
					pw.print(t + " ");
				}
				pw.println('.');
			}
		}
	}
}