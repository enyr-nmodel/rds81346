package org.nmodel;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.io.output.StringBuilderWriter;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.rulewerk.core.model.api.Rule;
import org.semanticweb.rulewerk.owlapi.OwlToRulesConveterEx;

public class OWLToRulewerk {
    
    protected final OWLOntologyManager ontologyManager;

    protected List<IRI> ontologies = new ArrayList<>();

    protected List<String> prefixStatements = new ArrayList<>();
    protected List<String> dataSourceStatements = new ArrayList<>();
    protected List<String> ruleStatements = new ArrayList<>();

    protected OwlToRulesConveterEx converter = new OwlToRulesConveterEx(false);
    
    public OWLToRulewerk(){

        /* ontology is loaded from a file using OWL API */
		ontologyManager = OWLManager.createOWLOntologyManager();
    }

    public void addOntology(IRI ontologyIri) throws OWLOntologyCreationException{
        ontologies.add(ontologyIri);
    }

    public void addOntology(String iri) throws OWLOntologyCreationException{
        addOntology(IRI.create(iri));
    }
    
    public void addPrefixStatement(String statement){
        prefixStatements.add(statement);
    }

    public void addDataSourceStatement(String statement){
        dataSourceStatements.add(statement);
    }

    public void addRuleStatement(String statement){
        ruleStatements.add(statement);
    }

    public List<OWLAxiom> getUnsupportedAxiomsSample() {
		return this.converter.getUnsupportedAxiomsSample();
	}

    public List<OWLAxiom> getAllUnsupportedAxioms(){
        return this.converter.getAllUnsupportedAxioms();
    }

    public String build() throws OWLOntologyCreationException{

        StringBuilderWriter w = new StringBuilderWriter();
        PrintWriter pw = new PrintWriter(w);

        for(IRI ontIri : ontologies){
            converter.addOntology(ontologyManager.loadOntologyFromOntologyDocument(ontIri));
        }

        for(String prefix : prefixStatements){
            pw.println(prefix);
        }

        pw.println();

        for(String dataSource : dataSourceStatements){
            pw.println(dataSource);
        }

        pw.println();

        for(Rule r : converter.getReifiedRules()){
            pw.println(r);
        }

        for(String rule : ruleStatements){
            pw.println(rule);
        }

        pw.close();

        return w.getBuilder().toString();
    }
}
