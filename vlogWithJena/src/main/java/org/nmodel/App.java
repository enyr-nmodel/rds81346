package org.nmodel;

import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.impl.ModelCom;
import org.semanticweb.rulewerk.core.reasoner.Algorithm;
import org.semanticweb.rulewerk.core.reasoner.CyclicityResult;
import org.semanticweb.rulewerk.core.reasoner.KnowledgeBase;
import org.semanticweb.rulewerk.reasoner.vlog.VLogReasoner;

import org.semanticweb.rulewerk.parser.RuleParser;

public class App 
{
	private static final String sparqlQuery = """
		PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
		PREFIX owl: <http://www.w3.org/2002/07/owl#>
		PREFIX iso1: <http://www.iso.org/iso81346-1#>
		PREFIX iso2: <http://www.iso.org/iso81346-2#>
		PREFIX iso10: <http://www.iso.org/iso81346-10#>
		PREFIX components: <http://www.enel.com/powerplant/components#>
		PREFIX systems: <http://www.enel.com/powerplant/systems#>
		PREFIX mainsystems: <http://www.enel.com/powerplant/mainsystems#>

		SELECT ?x ?z WHERE {
			systems:PowerPlantSystemLNC a ?z .
			?x a ?z
		}		
	""";
	
    public static void main( String[] args ) throws Exception
    {
		/* Convert OWL to rulewerk format and connect to trident triple store */
		OWLToRulewerk owlToRuleWerk = new OWLToRulewerk();
		owlToRuleWerk.addOntology("https://gitlab.com/enyr-nmodel/egp-ontology/-/raw/main/iso81346-1.owl");
		owlToRuleWerk.addOntology("https://gitlab.com/enyr-nmodel/egp-ontology/-/raw/main/iso81346-10.owl");
		owlToRuleWerk.addOntology("https://gitlab.com/enyr-nmodel/egp-ontology/-/raw/main/iso81346-2.owl");
		owlToRuleWerk.addOntology("https://gitlab.com/enyr-nmodel/egp-ontology/-/raw/main/enelPPMainSystems.owl");
		owlToRuleWerk.addOntology("https://gitlab.com/enyr-nmodel/egp-ontology/-/raw/main/enelPPComponents.owl");
		owlToRuleWerk.addOntology("https://gitlab.com/enyr-nmodel/egp-ontology/-/raw/main/enelPPSystems.owl");

		owlToRuleWerk.addDataSourceStatement(
			"@source TI[3]: trident(\"/home/marco/Source_Projects/vlog/build/trident_db/\") .");
		
		String ont = owlToRuleWerk.build();
		KnowledgeBase kb = RuleParser.parse(ont);
		
		try (VLogReasoner reasoner = new VLogReasoner(kb)) {

			// Check if materialization will terminate
            CyclicityResult res = reasoner.checkForCycles();
            if(res == CyclicityResult.CYCLIC || res == CyclicityResult.UNDETERMINED){
				System.out.println("Materialization might not terminate with given ontology!");
			}else {
				System.out.println("Materialization will terminate with given ontology!");
			}

			// Set materialization algorithm (RESTRICTED terminates in more cases)
			reasoner.setAlgorithm(Algorithm.RESTRICTED_CHASE);

			// Materialize in memory
			reasoner.reason();
			
			// Present the reasoned data to Jena ARQ engine as a Graph, so we can use
			// SPARQL using Jena API. The model should be created only once.
			Model model = new ModelCom(new VLogGraph(reasoner));

			//Convert the sparql query string to a Jena Query object
    		Query q = QueryFactory.create(sparqlQuery);

			//Combine the query to the model
    		QueryExecution qe = QueryExecutionFactory.create(q, model);

			//Execute the query, and get the result iterator
    		ResultSet results = qe.execSelect();

			//Print the results
			while(results.hasNext()){
				QuerySolution solution = results.next();
				System.out.println(solution);
			}
		}
    }
}
