package org.nmodel;

import java.util.List;

import org.apache.jena.graph.Node;
import org.apache.jena.graph.Triple;
import org.apache.jena.graph.impl.GraphBase;
import org.apache.jena.sparql.util.NodeFactoryExtra;
import org.apache.jena.util.iterator.ExtendedIterator;
import org.apache.jena.util.iterator.NiceIterator;
import org.semanticweb.rulewerk.core.model.api.QueryResult;
import org.semanticweb.rulewerk.core.model.api.Term;
import org.semanticweb.rulewerk.core.reasoner.QueryResultIterator;
import org.semanticweb.rulewerk.core.reasoner.Reasoner;

public class VLogGraph extends GraphBase{

    private Reasoner reasoner;
    public VLogGraph(Reasoner reasoner){
        this.reasoner = reasoner;
    }

    @Override
    protected ExtendedIterator<Triple> graphBaseFind(Triple triple) {
        String s = nodeToString(triple.getMatchSubject());
        String p = nodeToString(triple.getMatchPredicate());
        String o = nodeToString(triple.getMatchObject());

        QueryResultIterator it = ReasonerUtils.triplesIterator(reasoner, s, p, o);

        return new NiceIterator<Triple>(){
            @Override
            public boolean hasNext() {
                return it.hasNext();
            }

            @Override
            public Triple next() {
                return VLogGraph.queryResultToTriple(it.next());
            }
        };
    }

    private static Triple queryResultToTriple(QueryResult result){
        List<Term> terms = result.getTerms();
        Term sTerm = terms.get(0);
        Term pTerm = terms.get(1);
        Term oTerm = terms.get(2);

        Node sNode, pNode, oNode;

        //TODO: should check term types
        
        sNode = NodeFactoryExtra.parseNode(sTerm.toString());
        pNode = NodeFactoryExtra.parseNode(pTerm.toString());
        oNode = NodeFactoryExtra.parseNode(oTerm.toString());

        return Triple.create(sNode, pNode, oNode);
    }

    private static String nodeToString(Node node){
        if(node == null){
            return null;
        }

        if(node.isURI()){
            return "<"+node.toString()+">";
        }

        return node.toString(true);
    }
    
}
