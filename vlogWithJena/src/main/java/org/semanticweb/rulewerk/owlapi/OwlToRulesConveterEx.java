package org.semanticweb.rulewerk.owlapi;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.rulewerk.core.model.api.Conjunction;
import org.semanticweb.rulewerk.core.model.api.Fact;
import org.semanticweb.rulewerk.core.model.api.Literal;
import org.semanticweb.rulewerk.core.model.api.PositiveLiteral;
import org.semanticweb.rulewerk.core.model.api.Predicate;
import org.semanticweb.rulewerk.core.model.api.Rule;
import org.semanticweb.rulewerk.core.model.api.Term;
import org.semanticweb.rulewerk.core.model.implementation.Expressions;

public class OwlToRulesConveterEx extends OwlToRulesConverter{
    
    private static final String RDF_TYPE = "http://www.w3.org/1999/02/22-rdf-syntax-ns#type";

    public static final String REIFIED_PREDICATE_NAME = "TI";

    public static final Predicate REIFIED_PREDICATE = Expressions.makePredicate(REIFIED_PREDICATE_NAME, 3);
    
    private boolean myFailOnUnsupported;

    private List<OWLAxiom> unsupportedAxioms;
        
    public OwlToRulesConveterEx(boolean failOnUnsupported){
        super(failOnUnsupported);

        this.myFailOnUnsupported = failOnUnsupported;

        this.unsupportedAxioms = new ArrayList<>();
    }

    public OwlToRulesConveterEx() {
        this(true);
    }

    @Override
    public void addOntology(final OWLOntology owlOntology) {
        
		this.owlAxiomToRulesConverter.startNewBlankNodeContext();
        
		owlOntology.axioms().forEach(owlAxiom -> {
			try {
				owlAxiom.accept(this.owlAxiomToRulesConverter);
			} catch (OwlFeatureNotSupportedException e) {
				if (this.myFailOnUnsupported) {
					throw e;
				} else {
					this.unsupportedAxioms.add(owlAxiom);
				}
			}
		});
	}

    @Override
    public List<OWLAxiom> getUnsupportedAxiomsSample() {
		return this.unsupportedAxioms.subList(0, 10);
	}

    public List<OWLAxiom> getAllUnsupportedAxioms(){
        return this.unsupportedAxioms;
    }

    @Override
    public int getUnsupportedAxiomsCount() {
		return this.unsupportedAxioms.size();
	}

    public Set<Rule> getReifiedRules(){
        Set<Rule> rules = this.getRules();

        Set<Rule> newRules = new HashSet<>();
        
        /*
        List<Term> mapTerms = new ArrayList<>();
        mapTerms.add(Expressions.makeUniversalVariable("X"));
        mapTerms.add(Expressions.makeUniversalVariable("Y"));
        mapTerms.add(Expressions.makeUniversalVariable("Z"));

        PositiveLiteral headMap = Expressions.makePositiveLiteral("TI",mapTerms);
        PositiveLiteral bodyMap = Expressions.makePositiveLiteral("TE", mapTerms);

        newRules.add(Expressions.makeRule(headMap, bodyMap));
        */
        
        for(Rule r : rules){
            List<PositiveLiteral> newHead = convertConjunction(r.getHead()).
                                stream().map(l -> (PositiveLiteral)l ).collect(Collectors.toList());

            List<Literal> newBody = convertConjunction(r.getBody());

            Conjunction<PositiveLiteral> headConj = Expressions.makeConjunction(newHead);
            Conjunction<Literal> bodyConj = Expressions.makeConjunction(newBody);
            
            newRules.add(Expressions.makeRule(headConj,bodyConj));
        }
        return newRules;
    }

    public Set<Fact> getReifiedFacts(){
        Set<Fact> newFacts = new HashSet<>();

        for(Fact f : this.getFacts()){
            Literal newFact = convertAtom(f);
            newFacts.add(Expressions.makeFact(newFact.getPredicate(), newFact.getArguments()));
        }

        return newFacts;
    }
    
    private List<Literal> convertConjunction(Conjunction<? extends Literal> conj){

        List<Literal> newBody = new ArrayList<>();

        for(Literal l : conj){
            newBody.add(convertAtom(l));
        }

        return newBody;
    }

    private Literal convertAtom(Literal l){
        if(l.getArguments().size() > 2){
            throw new IllegalArgumentException("Rule contains non unary/binary predicate");
        }

        // class memnbership
        if(l.getArguments().size() == 1){
            Term individual = l.getArguments().get(0);
            List<Term> terms = new ArrayList<>();
            terms.add(individual);
            terms.add(Expressions.makeAbstractConstant(RDF_TYPE));
            terms.add(Expressions.makeAbstractConstant(l.getPredicate().getName()));
            Literal atom = null;
            if (l.isNegated())
                atom = Expressions.makeNegativeLiteral(REIFIED_PREDICATE_NAME,terms);
            else
                atom = Expressions.makePositiveLiteral(REIFIED_PREDICATE_NAME, terms);

            return atom;
        }else{ //property
            Term t1 = l.getArguments().get(0);
            Term t2 = l.getArguments().get(1);

            List<Term> terms = new ArrayList<>();
            terms.add(t1);
            terms.add(Expressions.makeAbstractConstant(l.getPredicate().getName()));
            terms.add(t2);

            Literal atom = null;
            if (l.isNegated())
                atom = Expressions.makeNegativeLiteral(REIFIED_PREDICATE_NAME,terms);
            else
                atom = Expressions.makePositiveLiteral(REIFIED_PREDICATE_NAME, terms);
                
            return atom;
        }
    }
}
