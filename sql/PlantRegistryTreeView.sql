WITH 
CDPlantTable AS
    (SELECT        *
      FROM            PLANT_REGISTRY
      WHERE        TYPE = 'CDPlant'), 
BL0Table AS
    (SELECT        *
      FROM            PLANT_REGISTRY
      WHERE        TYPE = 'BL0'), 
BL1TableSingle AS
    (SELECT        *
      FROM            PLANT_REGISTRY
      WHERE        TYPE = 'BL1' AND BL1 NOT LIKE '%.%'),
BL1TableDouble AS
    (SELECT        *
      FROM            PLANT_REGISTRY
      WHERE        TYPE = 'BL1' AND BL1 LIKE '%.%'), 
BL1Table AS
    (SELECT        *
      FROM            PLANT_REGISTRY
      WHERE        TYPE = 'BL1'), 
BL2Table AS
    (SELECT        *
      FROM            PLANT_REGISTRY
      WHERE        TYPE = 'BL2')

SELECT CDPlantTable.OBJ_ID, CDPlantTable.Type AS Level, CDPlantTable.CDPlant AS Designation, CDPlantTable.Description, CDPlantTable.Type, 'root' AS ParentID 
FROM CDPlantTable
UNION
SELECT BL0Table.OBJ_ID, BL0Table.Type AS Level, BL0Table.BL0 AS Designation, BL0Table.Description, BL0Table.Type, CDPlantTable.OBJ_ID AS ParentID
FROM BL0Table INNER JOIN CDPlantTable ON BL0Table.CDPlant = CDPlantTable.CDPlant
UNION
SELECT BL1TableSingle.OBJ_ID, BL1TableSingle.Type AS Level, BL1TableSingle.BL1 AS Designation, BL1TableSingle.Description, BL1TableSingle.Type, BL0Table.OBJ_ID AS ParentID
FROM BL1TableSingle INNER JOIN BL0Table ON BL1TableSingle.CDPlant = BL0Table.CDPlant AND BL1TableSingle.BL0 = BL0Table.BL0
UNION
SELECT BL1TableDouble.OBJ_ID, BL1TableDouble.Type AS Level, RIGHT(BL1TableDouble.BL1,5) AS Designation, BL1TableDouble.Description, BL1TableDouble.Type, BL1TableSingle.OBJ_ID AS ParentID
FROM BL1TableDouble INNER JOIN BL1TableSingle ON BL1TableDouble.CDPlant = BL1TableSingle.CDPlant AND BL1TableDouble.BL0 = BL1TableSingle.BL0 AND LEFT(BL1TableDouble.BL1, 5) = BL1TableSingle.BL1
UNION
SELECT BL2Table.OBJ_ID, BL2Table.Type AS Level, BL2Table.BL2 AS Designation, BL2Table.Description, BL2Table.Type, BL1Table.OBJ_ID AS ParentID
FROM BL2Table INNER JOIN BL1Table ON BL2Table.CDPlant = BL1Table.CDPlant AND BL2Table.BL0 = BL1Table.BL0 AND BL2Table.BL1 = BL1Table.BL1