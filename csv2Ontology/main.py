from owlready2 import *
import pandas as pd
import getopt, sys
 
 
csvfile = ""
owlfile = ""

# Remove 1st argument from the
# list of command line arguments
argumentList = sys.argv[1:]

print(argumentList)

# Options
options = "c:o:"
 
# Long options
long_options = ["Csv", "Ontology"]
 
try:
    # Parsing argument
    arguments, values = getopt.getopt(argumentList, options, long_options)
     
    # checking each argument
    for currentArgument, currentValue in arguments:
 
        if currentArgument in ("-c", "--Csv"):
            csvfile = currentValue
            print(("Loading % s .") % (csvfile))
             
        elif currentArgument in ("-o", "--Ontology"):
            owlfile = currentValue
            print(("Working on ontology % s .") % (owlfile))
             
except getopt.error as err:
    # output error, and return with an error code
    print (str(err))

#df = pd.read_csv("ComponentSystems81346-2.csv")
df = pd.read_csv(csvfile)
#print(df)    
print("CSV loaded...")

onto_path.append("../")

#onto = get_ontology("rds81346-2.owl").load()
onto = get_ontology(owlfile).load()
onto.load()

print("Ontology loaded...")
#print(onto['ComponentSystemObject'])

#with onto:
    #class Energy_transforming_system(onto.Power_supply_system):
    #    pass

#    class LetterCode(AnnotationProperty):
#        pass

# Energy_transforming_system.comment = ["power supply system transforming energy or energy carrier"]
# Energy_transforming_system.LetterCode = ["A"]

for i in df.index:
    if onto[df['SuperClass'][i]] is not None:
        NewClass = types.new_class(df['EntityName'][i], (onto[df['SuperClass'][i]],))
        NewClass.comment = [locstr(df['EntityDescription'][i], lang='en')]
        NewClass.label = [locstr(df['EntityLabel'][i], lang='en')]
        #NewClass.LetterCode = [df['LetterCode'][i]]
        print(NewClass.name)
    if onto[df['IndividualOf'][i]] is not None:
        # class_ref = globals()[df['IndividualOf'][i]]
        NewIndividual = onto[df['IndividualOf'][i]]()
        NewIndividual.name = df['EntityName'][i]
        NewIndividual.comment = [locstr(df['EntityDescription'][i], lang='en')]
        NewIndividual.label = [locstr(df['EntityLabel'][i], lang='en')]
        NewIndividual.LetterCode = [str(df['LetterCode'][i])]
        print(NewIndividual.name)

onto.save()

